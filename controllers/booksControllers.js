
//------------------------Get all the books from DB------------------------
const getAllBooks = async(req, res)=> {
    res.json("Get all books")
  }



  //------------------------Get a single book by id from DB------------------------
const getSingleBook =async(req, res)=> {
  res.json("Get a book")
}



//------------------------Add a books to DB------------------------
  const addBook = async (req, res) => {
    res.json("Add a book")
  }



  //------------------------Update a book by id from DB------------------------
  const updateAbook = async (req, res) => {
     res.json("Update a book")
  }


  //------------------------Delete a book by id from DB------------------------
  const deleteBook =async(req, res)=> {
     res.json("Delete a book")
  }


module.exports = {getAllBooks, getSingleBook, addBook, updateAbook,deleteBook}