const express = require('express')
const app = express()
const booksRoutes = require('./routes/booksRoutes')
const authorRoutes = require('./routes/authorRoutes')


const PORT = 3000

app.use(express.json());
app.use(express.urlencoded({ extended: true }));



app.use('/books', booksRoutes)
app.use('/authors',authorRoutes )




app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

