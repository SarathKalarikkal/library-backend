
//------------------------Get all the authors from DB------------------------
const getAllAuthors =async(req, res)=> {
    res.json("Get All authors")
  }



  //------------------------Get a author from DB------------------------
const getAuthor = async(req, res)=> {
  res.json("Get a authors")
  }



 //------------------------Add an author to DB------------------------ 
const addAuthor = async (req, res) => {
 res.json("Add a author")
};

//------------------------Update an existing author in DB------------------------
const updateAuthor = async (req, res) => {
 res.json("Update a author")
};
  //------------------------Delete a author from DB------------------------
const deleteAuthor = async(req, res)=> {
   res.json("Delete a author")
  }  
module.exports = {getAllAuthors, getAuthor, addAuthor, updateAuthor, deleteAuthor}  